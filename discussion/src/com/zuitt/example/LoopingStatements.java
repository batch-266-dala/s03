package com.zuitt.example;

public class LoopingStatements {
    public static void main(String[] args) {
//        Loops
//        -Control Structures that allows code blocks to be executed multiple times

//        While Loop
/*
        int x = 0;

        while (x < 10) {
            System.out.println("Loop Number: " + x);
            x++;
        }
*/
//        Do-While Loop

/*
        int y = 10;

        do {
            System.out.println("Countdown: " + y);
            y--;
        } while(y > 0);

*/
//        For Loop
//        Syntax:
//        for(initialValue; condition; iteration)
/*
            for(int i = 0; i < 10; i++){
            System.out.println("Current count: " + i);
        }
*/

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(int row = 0; row <= 2; row++){
            for(int col = 0; col <= 2; col++) {
                System.out.println(classroom[row][col]);
            }
        }
    }
}
