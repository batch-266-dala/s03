package com.zuitt.example;


import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
//        Try-catch-finally
        Scanner input = new Scanner(System.in);


        try {
            System.out.println("Enter a number: ");
            int num = input.nextInt();
            System.out.println("Enter a number 2: ");
            int num2 = input.nextInt();
            double quot = num/num2;

            System.out.println("The quotient is: " + quot);
        }
        catch (ArithmeticException e) {
            System.out.println("You cannot divide a whole number to 0");
        }
        catch (InputMismatchException e) {
            System.out.println("Please input numbers only");
        }
        catch (Exception e) {
            System.out.println("Something went wrong. Please try again");
        }
        finally {
            System.out.println("This will execute no matter what");
        }

    }
}
