package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {

        int count = 1;
        long result = 1;

        Scanner input = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int num = input.nextInt();
        try {
             if (num >= 1) {
                do {
                    result = result * count;
                    ++count;
                } while(count <= num);
            }
        }
        catch(InputMismatchException e){
            System.out.println("Only numbers are accepted!");
        }
        catch(Exception e) {
            System.out.println("Invalid input");
        }
        finally {
            System.out.println("The factorial of " + num + " is " + result);
        }
    }
}
